<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>

<html>
    <head>
        <title>
            <jsp:invoke fragment="title"/>
        </title>
        <link href="assets/css/style.css" type="text/css" rel="stylesheet"  />
        <link href="assets/css/bootstrap.css" type="text/css" rel="stylesheet"  />
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">
                    <img src="assets/images/logo.png"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
            <div id="header">
                <h1><jsp:invoke fragment="header"/></h1>
            </div>
            <div id="body">
                <jsp:doBody/>
            </div>
            <div id="footer">
                <hr/>
                <p class="text-center" id="copyright">Copyright CodeGym @ 2018.</p>
            </div>
        </div>
        <script src="assets/js/bootstrap.js" type="text/javascript"></script>
    </body>
</html>