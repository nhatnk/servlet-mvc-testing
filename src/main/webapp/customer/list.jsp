<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>

<t:page>
    <jsp:attribute name="title">Customer List</jsp:attribute>
    <jsp:attribute name="header">Customer List</jsp:attribute>
    <jsp:body>
        <a class="btn btn-secondary" href="/customers?action=create">Create new customer</a>
        <hr/>
        <table class="table">
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Address</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            <c:forEach items='${requestScope["customers"]}' var="customer">
                <tr>
                    <td><a href="/customers?action=view&id=${customer.getId()}">${customer.getName()}</a></td>
                    <td>${customer.getEmail()}</td>
                    <td>${customer.getAddress()}</td>
                    <td><a href="/customers?action=edit&id=${customer.getId()}">edit</a></td>
                    <td><a href="/customers?action=delete&id=${customer.getId()}">delete</a></td>
                </tr>
            </c:forEach>
        </table>
    </jsp:body>
</t:page>