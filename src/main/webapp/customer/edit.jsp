<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>

<t:page>
    <jsp:attribute name="title">Edit customer</jsp:attribute>
    <jsp:attribute name="header">Edit customer</jsp:attribute>
    <jsp:body>
        <c:if test='${requestScope["message"] != null}'>
            <div class="alert alert-success" role="alert">
                    ${requestScope["message"]}
            </div>
        </c:if>
        <a class="btn btn-secondary" href="/customers">Back to customer list</a>
        <hr/>
        <form method="post" class="form">
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name" id="name" value="${requestScope["customer"].getName()}" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="email" id="email" value="${requestScope["customer"].getEmail()}" required>
            </div>
            <div class="form-group">
                <label>Address</label>
                <input class="form-control" type="text" name="address" id="address" value="${requestScope["customer"].getAddress()}" required>
            </div>
            <input class="btn btn-primary" type="submit" value="Update customer">
        </form>
    </jsp:body>
</t:page>