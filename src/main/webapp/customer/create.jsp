<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>

<t:page>
    <jsp:attribute name="title">Create new customer</jsp:attribute>
    <jsp:attribute name="header">Create new customer</jsp:attribute>
    <jsp:body>
        <c:if test='${requestScope["message"] != null}'>
            <div class="alert alert-success" role="alert">
                    ${requestScope["message"]}
            </div>
        </c:if>
        <a class="btn btn-secondary" href="/customers">Back to customer list</a>
        <hr/>
        <form method="post" class="form">
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name" id="name" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="email" id="email" required>
            </div>
            <div class="form-group">
                <label>Address</label>
                <input class="form-control" type="text" name="address" id="address" required>
            </div>
            <input class="btn btn-primary" type="submit" value="Create customer">
        </form>
    </jsp:body>
</t:page>