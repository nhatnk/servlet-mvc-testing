<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--
  Created by IntelliJ IDEA.
  User: nhat
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>

<t:page>
    <jsp:attribute name="title">Deleting customer</jsp:attribute>
    <jsp:attribute name="header">Deleting customer</jsp:attribute>
    <jsp:body>
        <a class="btn btn-secondary" href="/customers">Back to customer list</a>
        <hr/>
        <form method="post">
            <h3>Are you sure?</h3>
            <fieldset>
                <legend>Customer information</legend>
                <table>
                    <tr>
                        <td>Name: </td>
                        <td>${requestScope["customer"].getName()}</td>
                    </tr>
                    <tr>
                        <td>Email: </td>
                        <td>${requestScope["customer"].getEmail()}</td>
                    </tr>
                    <tr>
                        <td>Address: </td>
                        <td>${requestScope["customer"].getAddress()}</td>
                    </tr>
                    <tr>
                        <td><input class="btn btn-primary" type="submit" value="Delete customer"></td>
                        <td><a class="btn btn-secondary" href="/customers">Back to customer list</a></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </jsp:body>
</t:page>
