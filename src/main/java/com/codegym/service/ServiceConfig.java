package com.codegym.service;

import com.codegym.repository.ConnectionUtils;
import com.codegym.repository.CustomerRepositoryImpl;

import java.sql.SQLException;

public class ServiceConfig {
    public static CustomerService getCustomerService() throws SQLException, ClassNotFoundException {
        return new CustomerServiceImpl(new CustomerRepositoryImpl(ConnectionUtils.connect()));
    }
}
