package com.codegym.service;

import com.codegym.model.Customer;
import com.codegym.repository.CustomerRepository;
import com.codegym.repository.CustomerRepositoryImpl;
import com.codegym.service.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomerServiceImplTest {
    @Mock
    private CustomerRepositoryImpl customerRepository;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @org.junit.jupiter.api.Test
    void findAll() {
        try {
            List<Customer> customers = new ArrayList<>();
            customers.add(new Customer(1, "John", "john@codegym.vn", "Hanoi"));
            CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
            Mockito.when(customerRepository.findAll()).thenReturn(customers);
            CustomerServiceImpl customerService = new CustomerServiceImpl(customerRepository);
            customerService.setCustomerRepository(customerRepository);
            List<Customer> expected = customerService.findAll();
            assertEquals(customers, expected);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    void save() {
        try {
            Customer customer = new Customer(1, "John", "john@codegym.vn", "Hanoi");
            CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
            Mockito.when(customerRepository.save(customer)).thenReturn(true);
            CustomerServiceImpl customerService = new CustomerServiceImpl(customerRepository);
            customerService.setCustomerRepository(customerRepository);
            boolean expected = customerService.save(customer);
            assertTrue(expected);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    void findById() {
        try {
            Customer customer = new Customer(1, "John", "john@codegym.vn", "Hanoi");
            CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
            Mockito.when(customerRepository.findById(1)).thenReturn(customer);
            CustomerServiceImpl customerService = new CustomerServiceImpl(customerRepository);
            customerService.setCustomerRepository(customerRepository);
            Customer expected = customerService.findById(1);
            assertEquals(customer, expected);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    void update() {
        try {
            Customer customer = new Customer(1, "John", "john@codegym.vn", "Hanoi");
            CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
            Mockito.when(customerRepository.update(1, customer)).thenReturn(true);
            CustomerServiceImpl customerService = new CustomerServiceImpl(customerRepository);
            customerService.setCustomerRepository(customerRepository);
            boolean expected = customerService.update(1, customer);
            assertTrue(expected);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    void remove() {
        try {
            Customer customer = new Customer(1, "John", "john@codegym.vn", "Hanoi");
            CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);
            Mockito.when(customerRepository.remove(1)).thenReturn(true);
            CustomerServiceImpl customerService = new CustomerServiceImpl(customerRepository);
            customerService.setCustomerRepository(customerRepository);
            boolean expected = customerService.remove(1);
            assertTrue(expected);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}