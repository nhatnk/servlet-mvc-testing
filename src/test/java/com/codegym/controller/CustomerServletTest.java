package com.codegym.controller;

import com.codegym.model.Customer;
import com.codegym.service.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class CustomerServletTest {
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private RequestDispatcher requestDispatcher;
    @Mock private CustomerServiceImpl customerService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void doGetEmptyListCustomers() throws Exception{
        List<Customer> customers = new ArrayList<>();

        when(customerService.findAll()).thenReturn(customers);
        when(request.getParameter("action")).thenReturn("list");
        when(request.getRequestDispatcher("customer/list.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(request).setAttribute("customers", customers);
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetListCustomers() throws Exception{
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(1, "John", "john@codegym.vn", "Vietnam"));

        when(customerService.findAll()).thenReturn(customers);
        when(request.getParameter("action")).thenReturn("list");
        when(request.getRequestDispatcher("customer/list.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(request).setAttribute("customers", customers);
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetShowCreateCustomerForm() throws Exception{
        when(request.getParameter("action")).thenReturn("create");
        when(request.getRequestDispatcher("customer/create.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doPostCreateCustomer() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");
        when(request.getParameter("action")).thenReturn("create");
        when(request.getParameter("name")).thenReturn("john");
        when(request.getParameter("email")).thenReturn("john@codegym.vn");
        when(request.getParameter("address")).thenReturn("Vietnam");
        when(customerService.save(customer)).thenReturn(true);
        when(request.getRequestDispatcher("customer/create.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doPost(request, response);

        verify(request).setAttribute("message", "New customer was created");
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetShowEditCustomerForm() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");

        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(customer);
        when(request.getRequestDispatcher("customer/edit.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(request).setAttribute("customer", customer);
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetShowEditCustomerFormNotFound() throws Exception{
        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(null);
        when(request.getRequestDispatcher("error-404.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doPostEditCustomer() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");
        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("name")).thenReturn("john2");
        when(request.getParameter("email")).thenReturn("john2@codegym.vn");
        when(request.getParameter("address")).thenReturn("Vietnam2");
        when(customerService.findById(1)).thenReturn(customer);
        when(customerService.update(1, customer)).thenReturn(true);
        when(request.getRequestDispatcher("customer/edit.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doPost(request, response);

        verify(request).setAttribute("message", "Customer information was updated");
        verify(request).setAttribute("customer", customer);
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doPostEditCustomerNotFound() throws Exception{
        when(request.getParameter("action")).thenReturn("edit");
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("name")).thenReturn("john2");
        when(request.getParameter("email")).thenReturn("john2@codegym.vn");
        when(request.getParameter("address")).thenReturn("Vietnam2");
        when(customerService.findById(1)).thenReturn(null);
        when(request.getRequestDispatcher("error-404.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doPost(request, response);

        verify(requestDispatcher).forward(request,response);
    }


    @Test
    void doGetShowDeleteCustomerForm() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");

        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(customer);
        when(request.getRequestDispatcher("customer/delete.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(request).setAttribute("customer", customer);
        verify(requestDispatcher).forward(request,response);
    }


    @Test
    void doGetShowDeleteCustomerFormNotFound() throws Exception{
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(null);
        when(request.getRequestDispatcher("error-404.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(requestDispatcher).forward(request,response);
    }


    @Test
    void doPostDeleteCustomer() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(customer);
        when(customerService.remove(1)).thenReturn(true);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doPost(request, response);

        verify(response).sendRedirect("/customers");
    }

    @Test
    void doPostDeleteCustomerNotFound() throws Exception{
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(null);
        when(request.getRequestDispatcher("error-404.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doPost(request, response);

        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetShowViewCustomerPage() throws Exception{
        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");

        when(request.getParameter("action")).thenReturn("view");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(customer);
        when(request.getRequestDispatcher("customer/view.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(request).setAttribute("customer", customer);
        verify(requestDispatcher).forward(request,response);
    }

    @Test
    void doGetShowViewCustomerPageNotFound() throws Exception{
        when(request.getParameter("action")).thenReturn("view");
        when(request.getParameter("id")).thenReturn("1");
        when(customerService.findById(1)).thenReturn(null);
        when(request.getRequestDispatcher("error-404.jsp")).thenReturn(requestDispatcher);

        CustomerServlet customerServlet = new CustomerServlet(customerService);
        customerServlet.doGet(request, response);

        verify(requestDispatcher).forward(request,response);
    }
}