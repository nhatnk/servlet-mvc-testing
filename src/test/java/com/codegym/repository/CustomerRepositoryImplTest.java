package com.codegym.repository;

import com.codegym.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomerRepositoryImplTest {

    @Mock private Connection connection;
    @Mock private PreparedStatement preparedStatement;
    @Mock private ResultSet resultSet;
    private boolean firstRow = true;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        firstRow = true;
    }

    @Test
    void findAll() throws Exception {
        when(connection.createStatement()).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery(any(String.class))).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(firstRow).then((a)->{firstRow = false; return false;});
        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("John");
        when(resultSet.getString("email")).thenReturn("john@codegym.vn");
        when(resultSet.getString("address")).thenReturn("Vietnam");

        CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl(connection);
        List<Customer> customers = customerRepository.findAll();
        assertEquals(customers.size(), 1);
    }

    @Test
    void save() throws SQLException, ClassNotFoundException {
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");
        CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl(connection);
        boolean result = customerRepository.save(customer);
        assertTrue(result);
    }

    @Test
    void findById() throws SQLException, ClassNotFoundException {
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(firstRow).then((a)->{firstRow = false; return false;});
        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("John");
        when(resultSet.getString("email")).thenReturn("john@codegym.vn");
        when(resultSet.getString("address")).thenReturn("Vietnam");

        CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl(connection);
        Customer customer = customerRepository.findById(1);
        assertNotNull(customer);
    }

    @Test
    void update() throws SQLException, ClassNotFoundException {
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        Customer customer = new Customer(1, "John", "john@codegym.vn", "Vietnam");
        CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl(connection);
        boolean result = customerRepository.update(1, customer);
        assertTrue(result);
    }

    @Test
    void remove() throws SQLException, ClassNotFoundException {
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl(connection);
        boolean result = customerRepository.remove(1);
        assertTrue(result);
    }
}